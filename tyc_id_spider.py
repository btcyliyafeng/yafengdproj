# -*- coding:utf-8 -*-
import re
import json
import scrapy
from commen.utils import server, apikey, inds, cities, get_redis_client, detail_task_list
from commen.logger_manager import LoggerManager


class TianyanchaIDSpider(scrapy.Spider):
    name = "id_spider"
    abnormal_task_key = "abnormal_tianyancha_crawl_id_task"
    large_num_task = "tianyancha_large_num_req"
    fmt_url = "http://%s/org/tianyancha?distance=1&type=index&size=20&apikey=%s&{}" % (server, apikey)
    logger = LoggerManager.get_logger(name)

    custom_settings = {
        'LOG_LEVEL': 'INFO',
        'CONCURRENT_REQUESTS': 10,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 10,
        'CONCURRENT_REQUESTS_PER_IP': 10,
        'RETRY_ENABLED': True,
        'RETRY_TIMES': 10,
        'RETRY_HTTP_CODES': [500, 502, 503, 504, 408, 404],
        'ITEM_PIPELINES': {'pipeline.IDMongoPipeline': 300}
    }

    def __init__(self, kw, pname, crawl_org=0, *args, **kwargs):
        super(TianyanchaIDSpider, self).__init__(*args, **kwargs)
        self.kw = kw
        self.pname = pname
        self.crawl_item = int(crawl_org)
        self.rds = get_redis_client()

    def start_requests(self):
        param = 'kw={}&sortType=1'.format(self.kw)
        url = self.fmt_url.format(param)
        self.logger.info('[pname]{}, [req]{}'.format(self.pname, url))
        yield scrapy.Request(url, self.parse, meta={'kw': self.kw, 'sort': 1, 'city': '', 'ind': '', 'pg': 1, 'tpg': -1})

    def parse(self, response):
        self.logger.info('[resp]{}'.format(response.url))
        kw=response.meta.get('kw')
        pg=response.meta.get('pg')
        tpg=response.meta.get('tpg')
        ind=response.meta.get('ind')
        city=response.meta.get('city')
        sort=response.meta.get('sort')
        try:
            data = json.loads(bytes.decode(response.body))
        except Exception:
            yield scrapy.Request(response.url, self.parse, meta=response.meta)
            return
        if data.get('retcode') == '100002': return
        if 'data' not in data :
            self.logger.info('请求失败, {}'.format(data))
            self.rds.lpush(self.abnormal_task_key, json.dumps(response.meta))
            return
        if data['count'] <= 200 or not (not city or cities.get(city)):  # 城市需要是二级城市, 或没有二级城市的一级城市
            if data['count'] > 200 and pg == 1: # 若添加了城市和行业, 仍超过 200 条 记录下来
                self.rds.lpush(self.large_num_task, response.url)
            for item in data['data']:
                item['_id'] = item['id']
                item.pop('id')
                if self.crawl_item and self.kw == re.sub(r"<.*?>", "", item["screenName"]):
                    dt = '{},{},{},0'.format(item['_id'], item['geoPoint']['lat'], item['geoPoint']['lon'])
                    self.rds.lpush(detail_task_list, dt)
                yield item
            if (data.get('hasNext') and pg < 5) and (tpg == -1 or int(data['pageToken']) < tpg):
                pagenum = data['pageToken']
                url = response.url
                if 'pageToken' in url:
                    url = re.sub('pageToken=.*', 'pageToken=' + str(pagenum), url)
                else:
                    url = url + '&pageToken=' + str(pagenum)
                yield scrapy.Request(url, self.parse, meta={'kw': kw, 'sort': sort, 'city': city, 'ind': ind, 'pg': pg + 1, 'tpg': tpg})
            elif sort == 1 and data['count'] > 100:  # 若正序翻页完毕, 且超过100条 反序排列抓取
                self.logger.info('meta[{}], num[{}], 数目在100-200之间, 反序排列抓取'.format(response.meta, data['count']))
                sort = 2
                pg_num = int((int(data['count']) - 100) / 20 + 1)
                params = 'kw={}&sortType={}&areaCode={}&category={}&pageToken={}'.format(kw, sort, city, ind, pg)
                url = self.fmt_url.format(params)
                yield scrapy.Request(url, self.parse, meta={'kw': kw, 'sort': sort, 'city': city, 'ind': ind, 'pg': pg, 'tpg': pg_num})
        else:   # 先利用行业拆分, 判断当前请求有无行业, 一级行业还是二级行业, 若是二级行业, 利用城市拆分请求
            self.logger.info('meta[{}], num[{}], 数目过大, 添加参数请求'.format(response.meta, data['count']))
            if not ind: # 没行业
                req_list = [dict(kw=kw, sort=sort, city=city, ind=i, pg=pg, tpg=tpg) for i in inds.keys()]
            else:       # 有行业
                if ind in inds and inds[ind]:     # 行业是一级行业, 并且此一级行业有二级行业
                    req_list = [dict(kw=kw, sort=sort, city=city, ind=i, pg=pg, tpg=tpg) for i in inds[ind]]
                else:   # 行业是二级行业, 或没有二级行业的一级行业
                    if city:    # 有城市, 能走到这儿, 肯定是有二级城市的一级城市
                        req_list = [dict(kw=kw, sort=sort, city='{}{:0>2}'.format(city, i + 1), ind=ind, pg=pg, tpg=tpg) for i in range(cities[city])]
                    else:       # 没有城市
                        req_list = [dict(kw=kw, sort=sort, city=c, ind=ind, pg=pg, tpg=tpg) for c in cities]

            for m in req_list:
                params = 'kw={}&sortType={}&areaCode={}&category={}&pageToken={}'.format(kw, sort, m['city'], m['ind'], pg)
                url = self.fmt_url.format(params)
                self.logger.info('[req]{}'.format(url))
                yield scrapy.Request(url, self.parse, meta=m)
