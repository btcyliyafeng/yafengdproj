# -*- coding:utf-8 -*-
import os
import json
import time
import inspect
import asyncio
import aiohttp
import pandas as pd


flow_type = {
    "2": "去过这里的人",
    # "3": "常住这里的人"
}

params_map = {
    "gender=1": "性别_男",
    "gender=2": "性别_女",
    "age=14,23": "年龄_14~23",
    "age=24,33": "年龄_24~33",
    "age=34,43": "年龄_34~43",
    "age=44,53": "年龄_44~53",
    "age=54,63": "年龄_54~63",
    "education=1": "学历_博士",
    "education=2": "学历_硕士",
    "education=3": "学历_本科",
    "education=4": "学历_高中",
    "education=5": "学历_初中",
    "education=6": "学历_小学",
    "education=7": "学历_专科",
    "asset=10000": "财产_有车",
    "asset=10001": "财产_有房",
    "living=0": "职业类型_在校大学生",
    "living=7006": "职业类型_IT互联网工作者",
    "living=7000": "职业类型_政府公职人员",
    "living=7001": "职业类型_科研教育者",
    "living=7009": "职业类型_医护工作者",
    "living=7002": "职业类型_金融工作者",
    "living=7003": "职业类型_法律工作者",
    "living=7010": "职业类型_销售",
    "living=7004": "职业类型_广告/市场/媒体/艺术工作者",
    "living=7005": "职业类型_房地产工作者",
    "living=7007": "职业类型_建筑工作者",
    "living=7013": "职业类型_服务业工作者",
    "living=7014": "职业类型_交通物流运输工作者",
    "living=7015": "职业类型_技工普工",
    "living=7016": "职业类型_客服/技术支持人员",
    "living=7017": "职业类型_贸易采购工作者",
    "living=7018": "职业类型_财务/人事/行政工作者",
    "living=7019": "职业类型_咨询顾问",
    "living=7020": "职业类型_能源与材料工作者",
    "living=7021": "职业类型_农林牧渔工作者",
    "living=8000": "职业属性_金领",
    "living=8001": "职业属性_白领",
    "living=8002": "职业属性_蓝领",
    "living=8003": "职业属性_中小企业管理人员",
    "interest=16": "兴趣_资讯控",
    "interest=83": "兴趣_生活服务重度用户",
    "interest=157": "兴趣_交通工具爱好者",
    "interest=164": "兴趣_休闲族",
    "interest=197": "兴趣_旅游控",
    "interest=205": "兴趣_游戏爱好者控",
    "interest=228": "兴趣_餐饮美食爱好者",
    "interest=245": "兴趣_保健达人",
    "interest=250": "兴趣_运动达人",
    "interest=286": "兴趣_美容美体达人",
    "interest=301": "兴趣_家居控",
    "interest=307": "兴趣_3C发烧友",
    "consumption_ability=1": "消费水平_高消费",
    "consumption_ability=2": "消费水平_低消费",
    "behavior=16&timeliness=6&strength=1": "行为_新闻资讯",
    "behavior=57&timeliness=6&strength=1": "行为_服饰鞋帽箱包",
    "behavior=61&timeliness=6&strength=1": "行为_教育",
    "behavior=83&timeliness=6&strength=1": "行为_生活服务",
    "behavior=124&timeliness=6&strength=1": "行为_金融",
    "behavior=156&timeliness=6&strength=1": "行为_商务服务",
    "behavior=157&timeliness=6&strength=1": "行为_交通",
    "behavior=164&timeliness=6&strength=1": "行为_娱乐休闲",
    "behavior=183&timeliness=6&strength=1": "行为_孕产育儿",
    "behavior=197&timeliness=6&strength=1": "行为_旅游",
    "behavior=203&timeliness=6&strength=1": "行为_房产",
    "behavior=205&timeliness=6&strength=1": "行为_游戏",
    "behavior=228&timeliness=6&strength=1": "行为_餐饮美食",
    "behavior=245&timeliness=6&strength=1": "行为_医疗健康",
    "behavior=250&timeliness=6&strength=1": "行为_体育运动",
    "behavior=286&timeliness=6&strength=1": "行为_美容/个护",
    "behavior=301&timeliness=6&strength=1": "行为_家居",
    "behavior=307&timeliness=6&strength=1": "行为_互联网/电子产品"
}

BASEURL = "http://api01.bitspaceman.com:8000/post/myapireq?apikey=yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9f" \
          "vMJZFgR2GaOxZoT&apiId=5&reqId=1&geo={lat},{lon}&radius=1000&flowType={flw}&{prm}"


class CrawlCount(object):

    def __init__(self):
        self.loop = asyncio.get_event_loop()  # 启动事件循环
        self.session = self.get_session(self.loop)
        self.curr = 0
        self.results = list()
        self.abnormals = list()
        self.root_dir = './hotel'

    def __del__(self):
        try:
            if inspect.iscoroutinefunction(self.session.close):
                loop = asyncio.get_event_loop()
                loop.run_until_complete(self.session.close())
            else:
                self.session.close()
        except:
            pass

    def get_session(self, loop=None, concurrency_limit=100):
        if not loop: loop = asyncio.get_event_loop()
        return aiohttp.ClientSession(connector=aiohttp.TCPConnector(limit=concurrency_limit, loop=loop), loop=loop)

    def get_geos_from_csv_file(self, file):
        enfile = os.path.join(self.root_dir, file)
        dt = pd.read_csv(enfile)
        dt = dt[['id', 'lat', 'lon']]
        return dt.set_index('id').T.to_dict()

    def save_result(self, file):
        file = 'result_' + file
        enfile = os.path.join(self.root_dir, file)
        with open(enfile, 'w') as f:
            f.write(json.dumps(self.results))

    async def fetch(self, url, hid, geo, flw_id, label, param):
        retry = 0
        success = False
        while not success and retry < 20:
            try:
                resp = await self.session.get(url)
                data = await resp.json()
            except Exception:
                retry += 1
            else:
                if data.get('data'):
                    tp = "_".join([flow_type[flw_id], label])
                    self.results.append({"hid": hid, "count": data['data'][0]["potential_user_count"], "type": tp})
                    tp = "_".join(["常住这里的人", label])
                    self.results.append({"hid": hid, "count": data['data'][0]["potential_user_count"], "type": tp})
                    success = True
                else:
                    retry += 1
        if not success:
            self.abnormals.append({"hid": hid, "geo": geo, "flw_id": flw_id, "param": param,  "label": label})
        self.curr -= 1

    async def start(self, geos):
        for hid, geo in geos.items():
            for param, lable in params_map.items():
                for flw_id, flw_name in flow_type.items():
                    while self.curr >= 20:
                        await asyncio.sleep(0.1)
                    url = BASEURL.format(lat=geo["lat"], lon=geo["lon"], flw=flw_id, prm=param)
                    asyncio.run_coroutine_threadsafe(self.fetch(url, hid, geo, flw_id, lable, param), self.loop)
                    self.curr += 1

        while self.curr > 0:
            await asyncio.sleep(0.1)

    async def fill_abnormal(self):
        abnormals, self.abnormals = self.abnormals, []
        for item in self.abnormals:
            while self.curr >= 20:
                await asyncio.sleep(0.1)
            url = BASEURL.format(lat=item["geo"]["lat"], lon=item["geo"]["lon"], flw=item["flw_id"], prm=item["param"])
            asyncio.run_coroutine_threadsafe(self.fetch(url, **item), self.loop)
            self.curr += 1


        while self.curr > 0:
            await asyncio.sleep(0.1)

    def run(self, geos=None, file=''):
        retry = 20
        if file:
            geos = self.get_geos_from_csv_file(file)
        self.loop.run_until_complete(self.start(geos))
        while self.abnormals and retry > 0:
            self.loop.run_until_complete(self.fill_abnormal())
            retry -= 1
        file = file if file else (str(int(time.time())) + '.csv')
        self.save_result(file)


if __name__ == '__main__':
    cc = CrawlCount()
    files = ['info_ctrip_1105.csv', 'ctrip_nearby_hotel_1105.csv']
    for file in files:
        cc.run(file=file)