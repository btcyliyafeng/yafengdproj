# -*- coding:utf-8 -*-
import time
import traceback
from pymongo import ASCENDING
from commen.logger_manager import logger_manager
from commen.utils import *


class GenTask(object):
    def __init__(self):
        self.size = 150
        self.rds = get_redis_client()
        self.task_key = detail_task_list
        self.logger = logger_manager.get_logger("detail_gen_task")
        self.mongo = get_mongo_client()

    @time_out(10)
    def gen_task(self, start):
        item = None
        self.logger.info("gen task from {}".format(start))
        rlt = self.mongo[tyc_id].find({"_id": {"$gt": start}}).sort('_id', direction=ASCENDING).limit(self.size)
        rds_pip = self.rds.pipeline()
        for item in rlt:
            dt = '{},{},{},0'.format(item['_id'], item['geoPoint']['lat'], item['geoPoint']['lon'])
            rds_pip.lpush(self.task_key, dt)
        if isinstance(item, dict) and '_id' in item:
            start = item['_id']
            rds_pip.hset(task_flag, detail_start, start)
        rds_pip.execute()
        self.logger.info("has gen task to {}".format(start))
        return start

    def run(self):
        ts, countor = time.time(), 0
        self.logger.info("Start gen crawl detail info task!")
        start = str(self.rds.hget(task_flag, detail_start).decode())
        while True:
            if time.time() - ts > 60 * 10:
                self.logger.info("---> speed {} item/min <---".format(int(countor/10)))
                ts, countor = time.time(), 0
            try:
                if self.rds.hget(task_flag, gen_detail_task_running) != b'0':
                    if self.rds.llen(self.task_key) > 100000:
                        time.sleep(3)
                        continue
                    try:
                        start = self.gen_task(start)
                        countor += self.size
                    except Exception as e:
                        self.logger.info("gen task time out, reason: {}".format(e))
                        self.mongo = get_mongo_client()
                        continue
                else:
                    self.logger.info("stop gen detail task")
                    time.sleep(60)
            except Exception:
                self.logger.error(traceback.format_exc())


if __name__ == '__main__':
    gt = GenTask()
    gt.run()