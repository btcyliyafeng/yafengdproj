# -*- coding:utf-8 -*-
import sys
import time
from scrapy.crawler import CrawlerProcess
from multiprocessing import Process, Queue
from tyc_id_spider import TianyanchaIDSpider
from commen.utils import id_task_key, concurrent, get_redis_client, task_flag
from commen.logger_manager import LoggerManager


rds = get_redis_client(0)
logger = LoggerManager.get_logger('main')
if len(sys.argv) > 1:
    concurrent = int(sys.argv[1])


def run(kw, pname):
    logger.info("{}, start[{}]".format(pname, kw))
    process = CrawlerProcess()
    process.crawl(TianyanchaIDSpider, kw, pname)
    process.start()
    logger.info("{}, end[{}]".format(pname, kw))
    pnamelist.put(pname)


if __name__ == '__main__':
    plist = []
    running = True
    start_time = time.time()
    pnamelist = Queue(maxsize=concurrent)
    for i in range(concurrent):
        pnamelist.put("P{}".format(i + 1))
    while rds.llen(id_task_key) > 0 and rds.hget(task_flag, 'tyc_crawl_id_running') != b'0':
        time.sleep(1)
        if len(plist) < concurrent:
            kw = rds.rpop(id_task_key)
            p = Process(target=run, args=(kw.decode(), pnamelist.get()))
            p.start()
            plist.append(p)
        for p in plist.copy():
            if not p.is_alive():
                plist.remove(p)