# -*- coding:utf-8 -*-
import re
import time
import inspect
import aiohttp
import asyncio
import traceback
from queue import Queue
from threading import Thread

from commen.utils import *
from commen.logger_manager import logger_manager


KEY_TP_MAP = {'mortgageCount': 'mortgage',
              'equityCount': 'equity',
              'abnormalCount': 'abnormal',
              'dishonest': 'dishonesty',
              'lawsuitCount': 'law',
              'courtCount': 'notice'}


class TYCRiskGenTask(object):

    def __init__(self):
        self.loop = self.start_even_loop()  # 启动事件循环
        self.session = self.get_session(self.loop)

        self.curr = 0  # 并发数
        self.task_key = risk_task_list
        self.bulk_items_num = bulk_items_num * 5
        self.abnormal_task_key = "tyc_gen_risk_abnormal_task"
        self.furl = "http://%s/risk/tianyancha?id={}&type=getCount&apikey=%s" % (server, apikey)

        self.ids_queue = Queue()
        self.buffer = Queue()
        self.rds = get_redis_client()
        self.mongo = get_mongo_client()
        self.logger = logger_manager.get_logger("detail_gen_task")
        self.svlogger = logger_manager.get_logger("save_risk_task")

    def __del__(self):
        try:
            if inspect.iscoroutinefunction(self.session.close):
                loop = asyncio.get_event_loop()
                loop.run_until_complete(self.session.close())
            else:
                self.session.close()
        except: pass

    @staticmethod
    def start_even_loop():
        def start_loop(loop):
            asyncio.set_event_loop(loop)
            loop.run_forever()

        event_loop = asyncio.new_event_loop()
        t = Thread(target=start_loop, args=(event_loop,))
        t.setDaemon(True)
        t.start()
        return event_loop

    def get_session(self, loop=None, concurrency_limit=100):
        if not loop: loop = asyncio.get_event_loop()
        return aiohttp.ClientSession(connector=aiohttp.TCPConnector(limit=concurrency_limit, loop=loop), loop=loop)

    def start_save_data(self):
        t = Thread(target=self._save_date)
        t.start()

    def _save_date(self):
        while True:
            if self.buffer.qsize() >= self.bulk_items_num:
                rds_items_buffer = [self.buffer.get() for i in range(self.bulk_items_num)]
            elif self.curr == 0 and self.buffer.qsize() > 0:
                rds_items_buffer = [self.buffer.get() for i in range(self.buffer.qsize())]
            else:
                time.sleep(1)
                continue
            try:
                self.save2rds(rds_items_buffer)
            except:
                self.logger.info("fail to save data")
                for tsk in rds_items_buffer:
                    self.buffer.put(tsk)

    def save2rds(self, buffer):
        rds_pipe = self.rds.pipeline()
        for tsk in buffer:
            rds_pipe.lpush(self.task_key, tsk)
        rds_pipe.execute()

    @time_out(10)
    def get_risk_id(self, start):
        self.logger.info("gen task from {}".format(start))
        query = {"_id": {"$gt": start}} if start else {}
        rlt = self.mongo[tyc_id].find(query).sort('_id', direction=1).limit(100)
        for item in rlt:
            kw = re.sub(r'<.*?>', '', item['screenName'])
            dt = '{},{},0'.format(item['_id'], kw)
            self.ids_queue.put(dt)
            start = item['_id']
        self.rds.hset(task_flag, risk_start, start)
        return start

    async def worker(self, tsk):
        oid, oname, retry = tsk.split(",")
        url = self.furl.format(oid)
        self.logger.info("crawl [{}] retry[{}]".format(url, retry))
        try:
            await self.crawl(oid, oname, url)
        except:
            if int(retry) < 4:
                self.ids_queue.put(",".join([oid, oname, str(int(retry) + 1)]))
            else:
                self.rds.lpush(self.abnormal_task_key, ",".join([oid, oname, str(int(retry) + 1)]))
                self.logger.info("give up task[{}]".format(oid))
            self.curr -= 1
            return
        self.logger.info("crawl success, task[{}]".format(tsk))
        self.curr -= 1

    async def crawl(self, oid, oname, url):
        resp = await self.session.get(url)
        data = await resp.json()
        if data["retcode"] != "000000":
            raise RuntimeError("retcode[{}] url[{}]".format(data["retcode"], url))
        item = data['data'][0]
        for k, v in KEY_TP_MAP.items():
            if item.get(k) and int(item[k]):
                tsk = '{}|{}|{}|1|0'.format(v, oid, oname)  # type|id|kw|pageToken|retry
                self.buffer.put(tsk)

    def start(self):
        self.start_save_data()
        countor = 0
        ts = time.time()
        start = str(self.rds.hget(task_flag, risk_start).decode())
        while True:
            if time.time() - ts > 60:
                self.logger.info("---> speed {} item/min <---".format(countor))
                ts, countor = time.time(), 0
            if self.buffer.qsize() > self.bulk_items_num * 10:
                time.sleep(0.5)
                self.logger.info("queue size too large, waiting for save".format(self.buffer.qsize()))
            if self.ids_queue.qsize() < 500 and self.rds.hget(task_flag, gen_risk_task_running) != b'0':
                start = self.get_risk_id(start)
            if self.curr < 150 and self.ids_queue.qsize() > 0:
                tsk = self.ids_queue.get()
                # asyncio.run_coroutine_threadsafe 接收一个协程对象和，事件循环对象
                asyncio.run_coroutine_threadsafe(self.worker(tsk), self.loop)
                self.logger.info("create task [{}] curr[{}]".format(tsk, self.curr))
                self.curr += 1  # 并发数加 1
                countor += 1
            else:
                time.sleep(0.1)


if __name__ == '__main__':
    sp = TYCRiskGenTask()
    sp.start()
