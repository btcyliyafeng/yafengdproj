# -*- coding:utf-8 -*-
import os
import sys
import logging
from logging.handlers import RotatingFileHandler


class LoggerManager(object):
    loggers = dict()

    fmt = '[%(asctime)s:LN%(lineno)d] %(message)s'
    path = os.path.dirname(os.path.dirname(__file__))
    log_dir = os.path.join(path, "logs")
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    @classmethod
    def _gen_logger(cls, name, level, fmt, m_per_log_file, backup_count, handler_type):
        backup_count = int(backup_count)
        logger = logging.getLogger(name)
        max_bytes = int(m_per_log_file) * 1204 * 1024
        log_file = "{}/{}.log".format(cls.log_dir, name)
        formater = logging.Formatter(fmt if fmt else cls.fmt)
        logger.setLevel(getattr(logging, level) if hasattr(logging, level) else logging.INFO)
        if int(handler_type) != 1:  # handler_type == 2
            stream_handler = logging.StreamHandler(sys.stdout)
            stream_handler.setFormatter(formater)
            logger.addHandler(stream_handler)
        if int(handler_type) != 2:  # handler_type == 1
            file_handler = RotatingFileHandler(log_file, maxBytes=max_bytes, backupCount=backup_count, encoding='utf-8')
            file_handler.setFormatter(formater)
            logger.addHandler(file_handler)
        return logger

    @classmethod
    def get_logger(cls, name, level='INFO', fmt=None, m_per_log_file=10, backup_count=3, handler_type=0):
        logger = cls.loggers.get(name)
        if not logger:
            logger = cls._gen_logger(name, level, fmt, m_per_log_file, backup_count, handler_type)
            cls.loggers[name] = logger
        return logger


logger_manager = LoggerManager()