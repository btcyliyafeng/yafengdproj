# -*- coding:utf-8 -*-
import redis
import signal
import pymongo
from elasticsearch import Elasticsearch
from motor.motor_asyncio import AsyncIOMotorClient
from commen.es_config import get_client


inds = {"A": [1, 2, 3, 4, 5],
        "B": [6, 7, 8, 9, 10, 11, 12],
        "C": [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43],
        "D": [44, 45, 46],
        "E": [47, 48, 49, 50],
        "F": [51, 52],
        "G": [53, 54, 55, 56, 57, 58, 59, 60],
        "H": [61, 62],
        "I": [63, 64, 65],
        "J": [66, 67, 68, 69],
        "K": [],
        "L": [71, 72],
        "M": [73, 74, 75],
        "N": [76, 77, 78],
        "O": [79, 80, 81],
        "P": [],
        "Q": [83, 84],
        "R": [85, 86, 87, 88, 89],
        "S": [90, 91, 92, 93, 94, 95],
        "T": []}


cities = {'11': 0, '12': 0, '13': 11, '14': 11, '15': 12, '21': 14, '22': 9, '23': 13, '31': 0, '32': 13, '33': 11,
          '34': 16, '35': 9, '36': 11, '37': 17, '41': 18, '42': 14, '43': 14, '44': 21, '45': 14, '46': 3, '50': 0,
          '51': 21, '52': 9, '53': 16, '54': 7, '61': 10, '62': 14, '63': 8, '64': 5, '65': 15, '71': 0, '81': 0, '82': 0}

concurrent = 10

# server
server = '10.26.222.219:8000'
# server = 'api01.bitspaceman.com:8000'
apikey = 'yPCg6ig17c811fD2EzCtm0oz7FE8BtCeS0vQxd9n6S4ZqBO9fvMJZFgR2GaOxZoT'


# redis
REDIS_HOST = dict(host="120.25.218.133", port=57305, password="FWxMkmaGK2Ar3RJDBXHk1PZNraCwkEzQ")

task_flag = "task_running_flag"
detail_start = "tyc_detail_start"
risk_start = "tyc_risk_start"
gen_detail_task_running = "tyc_gen_detail_task_running"
gen_risk_task_running = "tyc_gen_risk_task_running"

tyc_id = "tianyancha_id"
detail_task_list = "tyc_detail_task"
risk_task_list = "tyc_risk_task"
id_task_key = "tyc_crawl_id_task"


# mongo
bulk_items_num = 100
# MONGO_HOST = "mongodb://yafeng:yafeng@120.79.97.220:55555/yafeng_data"
MONGO_HOST = "mongodb://yafeng:yafeng@10.30.0.53:55555/yafeng_data"


# elasticsearch
ES_HOSTS = ['10.27.206.134:9900']


def get_redis_client(db=1):
    pool = redis.ConnectionPool(db=int(db), **REDIS_HOST)
    return redis.Redis(connection_pool=pool)


def get_mongo_client():
    client = pymongo.MongoClient(MONGO_HOST)
    return client['yafeng_data']


def get_async_mongo_client():
    client = AsyncIOMotorClient(MONGO_HOST)
    return client['yafeng_data']


def get_es_client():
    return Elasticsearch(hosts=ES_HOSTS, timeout=60)


def get_async_es_client():
    return get_client(ES_HOSTS, time_out=60)


def time_out(sec):

    def handler(signum, frame):
        raise TimeoutError

    def inner(fun):
        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, handler)
            signal.alarm(sec)
            result = fun(*args, **kwargs)
            signal.alarm(0)
            return result

        return wrapper

    return inner