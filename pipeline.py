# -*- coding:utf-8 -*-
import time
import traceback
from pymongo import UpdateOne
from commen.utils import get_mongo_client, bulk_items_num, tyc_id
from commen.logger_manager import LoggerManager


class IDMongoPipeline(object):

    def __init__(self):
        self.coll = get_mongo_client()
        self.logger = LoggerManager.get_logger('save_id')
        self.items_buffer = list()

    def process_item(self, item, spider):
        item['createDate'] = int(time.time())
        self.items_buffer.append(UpdateOne({'_id': item['_id']}, {'$set': item}, upsert=True))
        if len(self.items_buffer) >= bulk_items_num:
            self.save2mongo()
            self.items_buffer = list()
        return item

    def close_spider(self, spider):
        if len(self.items_buffer):
            self.save2mongo()

    def save2mongo(self):
        try:
            start = time.time()
            result = self.coll[tyc_id].bulk_write(self.items_buffer, ordered=False)
            rd = result.bulk_api_result
            ts = time.time() - start
            self.logger.info('bulk result: 插入[{}], 更新[{}], 失败[{}], 耗时[{}]S'.format(rd['nUpserted'], rd['nModified'], rd['writeErrors'], ts))
        except Exception:
            self.logger.error(traceback.format_exc())