# -*- coding:utf-8 -*-
import time
import inspect
import aiohttp
import asyncio
import traceback
from queue import Queue
from threading import Thread
from pymongo import UpdateOne
from elasticsearch import helpers

from commen.utils import *
from commen.logger_manager import logger_manager

FLDS = ["_id", "appCode", "id", "screenName", "screenNameEng", "legalRepresentative", "creditCode",
        "orgCode", "dataType", "createDate", "licenseNo", "address", "state", "city", "district",
        "geoPoint", "telephones", "businessStatus", "tags"]


class TYCDetailSpider(object):

    def __init__(self):
        self.loop = self.start_even_loop()  # 启动事件循环
        self.session = self.get_session(self.loop)

        self.curr = 0  # 并发数
        self.task_key = detail_task_list
        self.abnormal_task_key = "tyc_detail_abnormal_task"
        self.furl = "http://%s/org/tianyancha?id={}&apikey=%s" % (server, apikey)

        self.buffer = Queue()
        self.es = get_es_client()
        self.rds = get_redis_client()
        self.mongo = get_mongo_client()
        self.logger = logger_manager.get_logger("risk_spider")
        self.svlogger = logger_manager.get_logger("save_risk_info")

    def __del__(self):
        try:
            if inspect.iscoroutinefunction(self.session.close):
                loop = asyncio.get_event_loop()
                loop.run_until_complete(self.session.close())
            else:
                self.session.close()
        except: pass

    @staticmethod
    def start_even_loop():
        def start_loop(loop):
            asyncio.set_event_loop(loop)
            loop.run_forever()

        event_loop = asyncio.new_event_loop()
        t = Thread(target=start_loop, args=(event_loop,))
        t.setDaemon(True)
        t.start()
        return event_loop

    def start_save_data(self):
        t = Thread(target=self._save_date)
        t.start()

    def get_session(self, loop=None, concurrency_limit=100):
        if not loop: loop = asyncio.get_event_loop()
        return aiohttp.ClientSession(connector=aiohttp.TCPConnector(limit=concurrency_limit, loop=loop), loop=loop)

    async def worker(self, tsk):
        oid, lat, lon, retry = tsk.split(",")
        url = self.furl.format(oid)
        self.logger.info("crawl [{}] retry[{}]".format(url, retry))
        try:
            item = await self.crawl(url)
        except:
            if int(retry) < 4:
                self.rds.lpush(self.task_key, ",".join([oid, lat, lon, str(int(retry) + 1)]))
            else:
                self.rds.lpush(self.abnormal_task_key, ",".join([oid, lat, lon, str(int(retry) + 1)]))
                self.logger.info("give up task[{}]".format(oid))
            self.curr -= 1
            return
        self.logger.info("crawl success, task[{}]".format(tsk))
        item['geoPoint'] = {"lat": float(lat), "lon": float(lon)}
        self.buffer.put(item)
        self.curr -= 1

    async def crawl(self, url):
        resp = await self.session.get(url)
        data = await resp.json()
        if data["retcode"] != "000000":
            raise RuntimeError("retcode[{}] url[{}]".format(data["retcode"], url))
        data["data"][0]["appCode"] = data["appCode"]
        data["data"][0]["dataType"] = data["dataType"]
        return data["data"][0]

    def _save_date(self):
        while True:
            if self.buffer.qsize() >= bulk_items_num:
                size = bulk_items_num
            elif self.curr == 0 and self.buffer.qsize() > 0:
                size = self.buffer.qsize()
            else:
                time.sleep(1)
                continue
            es_items_buffer = list()
            mongo_items_buffer = list()
            for i in range(size):
                item = self.buffer.get()
                item['_id'] = item['id']
                item['createDate'] = int(time.time())
                es_items_buffer.append({k: v for k, v in item.items() if k in FLDS})
                mongo_items_buffer.append(UpdateOne({'_id': item['_id']}, {'$set': item}, upsert=True))
            try:
                self.save2mongo(mongo_items_buffer)
                self.save2es(es_items_buffer)
            except:
                self.logger.info("fail to save data")
                rds_pipe = self.rds.pipeline()
                for item in es_items_buffer:
                    tsk = ",".join([item['id'], item['geoPoint']['lat'], item['geoPoint']['log'], '0'])
                    rds_pipe.lpush(self.abnormal_task_key, tsk)
                rds_pipe.execute()

    def save2mongo(self, buffer):
        try:
            start = time.time()
            coll_name = self.rds.hget(task_flag, "save_obj_name").decode()
            result = self.mongo[coll_name].bulk_write(buffer, ordered=False)
            rd = result.bulk_api_result
            ts = time.time() - start
            self.svlogger.info('bulk result: insert[{}], update[{}], fail[{}], use[{}]s'.format(
                rd['nUpserted'], rd['nModified'], rd['writeErrors'], ts))
        except Exception:
            self.svlogger.error(traceback.format_exc())

    def save2es(self, buffer):
        index_name = self.rds.hget(task_flag, "save_obj_name").decode()
        items_buffer = [{
            '_id': item.pop('_id'),
            '_op_type': 'index',
            '_index': index_name,
            '_type': item.pop('dataType'),
            '_source': item
        } for item in buffer]
        try:
            rlt = helpers.bulk(self.es, items_buffer, raise_on_error=False)
            self.svlogger.info('es bulk result: {}'.format(rlt))
        except Exception:
            self.svlogger.error(traceback.format_exc())

    def create_task(self):
        countor = 0
        ts = time.time()
        while True:
            if time.time() - ts > 60:
                self.logger.info("---> speed {} item/min <---".format(countor))
                ts, countor = time.time(), 0
            if self.buffer.qsize() > bulk_items_num * 10:
                time.sleep(1)
                self.logger.info("queue size too large, waiting for save".format(self.buffer.qsize()))
            if self.curr < 100:
                try:
                    tsk = self.rds.rpop(self.task_key).decode()
                except:
                    self.logger.info('No task, wait 60s')
                    time.sleep(60)
                else:
                    # asyncio.run_coroutine_threadsafe 接收一个协程对象和，事件循环对象
                    asyncio.run_coroutine_threadsafe(self.worker(tsk), self.loop)
                    self.logger.info("create task [{}] curr[{}]".format(tsk, self.curr))
                    self.curr += 1  # 并发数加 1
                    countor += 1
            else:
                time.sleep(0.1)

    def start(self):
        self.start_save_data()
        t = Thread(target=self.create_task)
        t.start()


if __name__ == '__main__':
    sp = TYCDetailSpider()
    sp.start()
