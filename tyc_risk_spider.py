# -*- coding:utf-8 -*-
import time
import inspect
import aiohttp
import asyncio
import traceback
from queue import Queue
from threading import Thread
from pymongo import UpdateOne

from commen.utils import *
from commen.logger_manager import logger_manager


class TYCRiskSpider(object):

    def __init__(self):
        self.loop = self.start_even_loop()  # 启动事件循环
        self.session = self.get_session(self.loop)

        self.curr = 0  # 并发数
        self.task_key = risk_task_list
        self.abnormal_task_key = "tyc_risk_abnormal_task"
        self.furl = "http://%s/risk/tianyancha?apikey=%s&type={}&id={}&kw={}&pageToken={}" % (server, apikey)

        self.buffer = Queue()
        self.tsk_queue = Queue()
        self.rds = get_redis_client()
        self.mongo = get_mongo_client()
        self.logger = logger_manager.get_logger("risk_spider")
        self.svlogger = logger_manager.get_logger("save_risk")

    def __del__(self):
        try:
            if inspect.iscoroutinefunction(self.session.close):
                loop = asyncio.get_event_loop()
                loop.run_until_complete(self.session.close())
            else:
                self.session.close()
        except: pass

    @staticmethod
    def start_even_loop():
        def start_loop(loop):
            asyncio.set_event_loop(loop)
            loop.run_forever()

        event_loop = asyncio.new_event_loop()
        t = Thread(target=start_loop, args=(event_loop,))
        t.setDaemon(True)
        t.start()
        return event_loop

    def get_session(self, loop=None, concurrency_limit=70):
        if not loop: loop = asyncio.get_event_loop()
        return aiohttp.ClientSession(connector=aiohttp.TCPConnector(limit=concurrency_limit, loop=loop), loop=loop)

    def start_save_data(self):
        t = Thread(target=self._save_date)
        t.start()

    def _save_date(self):
        while True:
            if self.buffer.qsize() >= bulk_items_num:
                size = bulk_items_num
            elif self.curr == 0 and self.buffer.qsize() > 0:
                size = self.buffer.qsize()
            else:
                time.sleep(1)
                continue
            temp_items_buffer = list()
            mongo_items_buffer = list()
            for i in range(size):
                item = self.buffer.get()
                temp_items_buffer.append(item)
                item['_id'] = item['id']
                item['createDate'] = int(time.time())
                mongo_items_buffer.append(UpdateOne({'_id': item['_id']}, {'$set': item}, upsert=True))
            try:
                self.save2mongo(mongo_items_buffer)
            except:
                self.logger.info("fail to save data")
                for item in temp_items_buffer:
                    self.buffer.put(item)

    def save2mongo(self, buffer):
        try:
            start = time.time()
            coll_name = self.rds.hget(task_flag, "save_obj_name").decode()
            coll_name = coll_name.replace('org', 'risk')
            result = self.mongo[coll_name].bulk_write(buffer, ordered=False)
            rd = result.bulk_api_result
            ts = time.time() - start
            self.svlogger.info('bulk result: insert[{}], update[{}], fail[{}], use[{}]s'.format(
                rd['nUpserted'], rd['nModified'], rd['writeErrors'], ts))
        except Exception:
            self.svlogger.error(traceback.format_exc())

    async def worker(self, tsk):
        tp, oid, kw, pg, retry = tsk.split("|")
        url = self.furl.format(tp, oid, kw, pg)
        self.logger.info("crawl [{}] retry[{}]".format(url, retry))
        try:
            items, npg = await self.crawl(url)
        except:
            tsk = "|".join([tp, oid, kw, pg, str(int(retry) + 1)])
            if int(retry) < 4:
                self.rds.lpush(self.task_key, tsk)
            else:
                self.rds.lpush(self.abnormal_task_key, tsk)
                self.logger.info("give up task[{}]".format(tsk))
            self.curr -= 1
            return
        self.logger.info("crawl success, task[{}]".format(tsk))
        if npg:
            self.tsk_queue.put("|".join([tp, oid, kw, npg, '0']))
        for item in items:
            item['type'] = tp
            self.buffer.put(item)
        self.curr -= 1

    async def crawl(self, url):
        resp = await self.session.get(url)
        data = await resp.json()
        if data["retcode"] != "000000":
            raise RuntimeError("retcode[{}] url[{}]".format(data["retcode"], url))
        data["data"][0]["appCode"] = data["appCode"]
        data["data"][0]["dataType"] = data["dataType"]
        return data["data"], data["pageToken"]

    def create_task(self):
        countor = 0
        ts = time.time()
        while True:
            if time.time() - ts > 60:
                self.logger.info("---> speed {} item/min <---".format(countor))
                ts, countor = time.time(), 0
            if self.buffer.qsize() > bulk_items_num * 10:
                time.sleep(1)
                self.logger.info("queue size[{}] too large, wait 1s ".format(self.buffer.qsize()))
            if self.curr < 70:
                tsk = ''
                if self.tsk_queue.qsize() > 0:
                    tsk = self.tsk_queue.get()
                else:
                    try:
                        tsk = self.rds.rpop(self.task_key).decode()
                    except:
                        self.logger.info('No task, wait 60s')
                        time.sleep(60)
                if tsk:
                    # asyncio.run_coroutine_threadsafe 接收一个协程对象和，事件循环对象
                    asyncio.run_coroutine_threadsafe(self.worker(tsk), self.loop)
                    self.logger.info("create task [{}] curr[{}]".format(tsk, self.curr))
                    self.curr += 1  # 并发数加 1
                    countor += 1
            else:
                time.sleep(0.1)

    def start(self):
        self.start_save_data()
        t = Thread(target=self.create_task)
        t.start()


if __name__ == '__main__':
    sp = TYCRiskSpider()
    sp.start()
