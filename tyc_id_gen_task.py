# -*- coding:utf-8 -*-
from commen.utils import get_redis_client, id_task_key
from commen.logger_manager import logger_manager


class GenTask(object):
    def __init__(self):
        self.kw_seeds_key = "tianyancha_kw"
        self.logger = logger_manager.get_logger("crawl_id_gen_task")
        self.rds = get_redis_client()
        self.rds_pip = self.rds.pipeline()

    def gen_cralw_id_task(self):
        num, total = 0, 0
        self.logger.info('start gen task ...')
        seeds_key = self.kw_seeds_key
        for i in self.rds.sscan_iter(seeds_key):
            i = i.decode().strip()
            self.rds_pip.lpush(id_task_key, i)
            num += 1
            total += 1
            if num >= 1000:
                self.rds_pip.execute()
                num = 0
        self.rds_pip.execute()
        self.logger.info('finashed, task num[{}] '.format(total))


if __name__ == '__main__':
    gt = GenTask()
    gt.gen_cralw_id_task()
