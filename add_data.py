# -*- coding:utf-8 -*-
import time
import argparse
from scrapy.crawler import CrawlerProcess
from multiprocessing import Process, Queue
from tyc_id_spider import TianyanchaIDSpider
from commen.utils import get_redis_client
from commen.logger_manager import LoggerManager


rds = get_redis_client(0)
logger = LoggerManager.get_logger('main')


def parse_args():
    parser = argparse.ArgumentParser(description="Add the specified company to the database.")
    parser.add_argument("-c", "--concurrent", help='Number of child processes', type=int, default=5)
    parser.add_argument("-cn", "--company-names", help='<Required> Company names, separated by comma', required=True)
    return parser.parse_args()


def run(kw, pname, crawl_match_kw):
    logger.info("{}, start[{}]".format(pname, kw))
    process = CrawlerProcess()
    process.crawl(TianyanchaIDSpider, kw, pname, crawl_match_kw)
    process.start()
    logger.info("{}, end[{}]".format(pname, kw))
    pnamelist.put(pname)


def main(orgs):
    plist = []
    crawl_match_kw = '1'
    for i in range(concurrent):
        pnamelist.put("P{}".format(i + 1))
    for kw in orgs:
        time.sleep(1)
        if len(plist) < concurrent:
            p = Process(target=run, args=(kw, pnamelist.get(), crawl_match_kw))
            p.start()
            plist.append(p)
        for p in plist.copy():
            if not p.is_alive():
                plist.remove(p)


if __name__ == '__main__':
    args = parse_args()
    concurrent = args.concurrent
    pnamelist = Queue(maxsize=concurrent)
    orgs = [i.strip() for i in args.company_names.split(',') if i]
    main(orgs)